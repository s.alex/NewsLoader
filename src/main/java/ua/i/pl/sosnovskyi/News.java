package ua.i.pl.sosnovskyi;

import java.util.List;

/**
 * Created by A Sosnovskyi on 03.09.2017.
 *
 */
public interface News {
    /**
     *
     * @return
     */
     List<Article> getArticles();
    Article getArticle(int i);
}
