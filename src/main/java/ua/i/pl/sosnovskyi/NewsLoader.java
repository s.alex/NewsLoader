package ua.i.pl.sosnovskyi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by A Sosnovskyi on 02.09.2017.
 */
class NewsLoader {

    private String urlString;


    private NewsLoader(String urlString) {
        this.urlString = urlString;

    }


    public static NewsLoader instanseOf(String urlString) {
        if (urlString.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return new NewsLoader(urlString);
    }


    private URL sendRequest(String urlString) throws MalformedURLException {
        return new URL(urlString);
    }


    private String getRequestString(URL url) {
        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "windows-1251"))) {
            String currentStr = null;
            while ((currentStr = br.readLine()) != null) {
                stringBuilder.append(currentStr);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }


    public News decode(News newsImpl) {
        String gsonString = null;
        try {
            URL uRl = this.sendRequest(urlString);
            gsonString = this.getRequestString(uRl);
        } catch (MalformedURLException e) {

            throw new RuntimeException(e);
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        News news = gson.fromJson(gsonString, newsImpl.getClass());
        return news;
    }
}
