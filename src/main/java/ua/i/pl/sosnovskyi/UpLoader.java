package ua.i.pl.sosnovskyi;

import java.io.*;

import java.util.Date;

import java.util.List;

/**
 * Created by A Sosnovskyi on 03.09.2017.
 */
class UpLoader {
    public static void writeToFile(List<? extends Article> articles) {
        StringBuilder sb = new StringBuilder();
        for (Article article : articles) {
            sb.append("\n" + article.getAuthor() + "\n").append(article.getTitle() + "\n").append(article.getUrl() + "\n")
                    .append(article.getDescription() + "\n" + "--------------------------------------------------------------");
        }

        String name = "News_".concat(Long.toString(new Date().getTime())).concat(".txt");
        String tmp = sb.toString();
        try (BufferedWriter br = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(name, true)))) {
            br.write(tmp);
            br.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println(name);
//        System.out.println(tmp);
    }

}
