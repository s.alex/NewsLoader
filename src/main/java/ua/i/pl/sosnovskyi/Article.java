package ua.i.pl.sosnovskyi;

import lombok.Getter;

import java.util.Date;

/**
 * Created by A Sosnovskyi on 03.09.2017.
 */

@Getter
class Article {
    protected final String author;
    protected final String title;
    protected final String description;
    protected final String url;
    protected final String urlToImage;
    protected Date publishedAt;

    Article(String author, String title, String description, String url, String urlToImage, Date publishedAt) {

        this.author = author;
        this.title = title;
        this.description = description;
        this.url = url;
        this.urlToImage = urlToImage;
        this.publishedAt = publishedAt;
    }

    @Override
    public String toString() {
        return "Article{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", urlToImage='" + urlToImage + '\'' +
                ", publishedAt=" + publishedAt +
                '}';
    }
}
