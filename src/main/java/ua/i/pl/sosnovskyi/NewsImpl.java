package ua.i.pl.sosnovskyi;

import lombok.Getter;

import java.util.*;

/**
 * Created by A Sosnovskyi on 02.09.2017.
 */
class NewsImpl implements News {
    private String status;
    private String source;
    private String sortBy;
    private Article[] articles;

    @Override
    public List<Article> getArticles() {
        return  new ArrayList(Arrays.asList(articles));
    }
    @Override
    public Article getArticle(int i) {
        Article result=null;
        result=articles[i];
        return result;
    }

}
