package ua.i.pl.sosnovskyi;

import java.net.MalformedURLException;

/**
 * Created by A Sosnovskyi on 02.09.2017.
 * ТЗ для тестового задания по JAVA.

 Результат желательно предоставить в виде проекта на гитхабе.

 Необходимо сделать приложение без ГУИ для загрузки и сохранения новостей в файл.

 При запуске Jar приложение должно выполнить запрос для получения новостей, распарсить json-ответ и сохранить полученные данные в файл вида:

 {Автор новости}
 {Название новости}
 {ссылка на новость}
 {описание новости}
 ____________________________________
 {Автор новости}
 {Название новости}
 {ссылка на новость}
 {описание новости}
 ____________________________________

 и т.д.

 Что можно использовать:
 •	для запроса нужно использовать классы: java.net.URL, java.net.HttpURLConnection;
 •	для записи в файл использовать стандартное java-API;
 •	для парсинга json можно воспользоваться библиотекой gson: https://github.com/google/gson.

 Запрос для получения новостей: https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=c7aee9c5be0349579444d8ac307620ea

 */
public class NewsLoaderApp {
    static String url = "https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=c7aee9c5be0349579444d8ac307620ea";


    public static void main(String[] args) {
        NewsLoader newsLoader = NewsLoader.instanseOf(url);
        News news = newsLoader.decode(new NewsImpl());
        UpLoader.writeToFile(news.getArticles());

    }
}
